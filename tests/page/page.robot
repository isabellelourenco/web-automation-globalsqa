***Settings***
Documentation    Validate Site Behavior

Resource        ${EXECDIR}/resources/base.robot

Suite Setup       Start Browser Session   
Test Teardown     Take Screenshot

***Test Cases***
Scenario: Validate Access to JMeter Training Page
    [tags]              page
    Access Home Page From Site
    Click on Jmeter Training Banner 
    Check if Page Contains Text "JMeter Tool Training Overview"               

    [Teardown]      Thinking and Take Screenshot        5       ${TEXT_LOCATOR} 

Scenario: Validade Search For Jmeter Training
    [tags]              search
    Access Home Page From Site
    Search for Jmeter Training
    Click on Jmeter Training Result
    Check if Page Contains Title "Jmeter Training"

    [Teardown]      Thinking and Take Screenshot        5       ${PAGE_TITLE}

