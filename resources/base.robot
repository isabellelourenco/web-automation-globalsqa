***Settings***
Documentation       Base File to Supports Automated Tests

Library     Browser
Library     String

Resource           actions/page.robot

***Keywords***
Start Browser Session           

    New Browser         ${browser}    ${headless}
    New Page            about:blank

HighLight Element 
    [Arguments]             ${XPATH}

    ${ELEMENT} =            Get Element             ${XPATH}      

    Execute JavaScript      (elem) => elem.setAttribute('style','background: yellow; border: 2px solid red;')          ${ELEMENT}

Thinking and Take Screenshot
    [Arguments]             ${TIMEOUT}          ${TEXT_LOCATOR} 

    Sleep                   ${TIMEOUT}
    HighLight Element       ${TEXT_LOCATOR} 
    Take Screenshot

    