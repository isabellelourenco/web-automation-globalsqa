***Settings***
Documentation       Site Actions

***Variables***
${HOME_URL}                     https://www.globalsqa.com/   
${SEARCH_BUTTON_LOCATOR}        //button[contains(@class,'button_search')]
${TEXT_LOCATOR}                 css=p strong
${JMETER_BANNER}                //h3[contains(.,'JMeter Training')]
${EXPECTED_JMETER_URL}          https://www.globalsqa.com/training/jmeter-training/
${JMETER_TRAINING}              //a[contains(.,'JMeter Training')]
${SEARCH_BAR}                   //input[contains(@id,'s')]
${PAGE_TITLE}                   //h1[contains(.,'JMeter Training')]


***Keywords***
Search For Element
    [Arguments]         ${ELEMENT}                ${TEXT_TO_SEARCH}

    Fill Text           ${ELEMENT}                ${TEXT_TO_SEARCH}  
    Click               ${SEARCH_BUTTON_LOCATOR}    

## Buttons & Links  
Click on Element 
    [Arguments]         ${ELEMENT} 

    Click                       ${ELEMENT} 

## Validations
Should Contains Text On Page 
    [Arguments]         ${TEXT_LOCATOR}           ${EXPECTED_TEXT} 

    ${PAGE_TEXT} =              Get Text                ${TEXT_LOCATOR}
    ${WORDS_LIST} =             Split String            ${PAGE_TEXT}            
    ${TEXT_TO_COMPARE} =        Evaluate                " ".join(${WORDS_LIST})     

    Should Be Equal             ${TEXT_TO_COMPARE}       ${EXPECTED_TEXT} 

Should Validate Title 
    [Arguments]         ${LOCATOR}          ${EXPECTED_TEXT}

    Get Text            ${LOCATOR}      should be    ${EXPECTED_TEXT}
      
## ---- STEPS
# Scenario: Validate Access to JMeter Training Page------------------------------------------------------------------------------
Access Home Page From Site
    Go To               ${HOME_URL}

Click on Jmeter Training Banner
    Click on Element                ${JMETER_BANNER} 

Check if Page Contains Text "JMeter Tool Training Overview"
    Scroll To                       ${TEXT_LOCATOR}
    Should Contains Text On Page    ${TEXT_LOCATOR}     JMeter Tool Training Overview 

# Scenario: Validade Search For Jmeter Training------------------------------------------------------------------------------    
Search for Jmeter Training
    Search For Element              ${SEARCH_BAR}       Jmeter Training 

Click on Jmeter Training Result
    Click On Element                ${JMETER_TRAINING} 

Check if Page Contains Title "Jmeter Training"
    Should Contains Text On Page    ${PAGE_TITLE}       JMeter Training
