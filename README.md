# Automação Web do Site Globalsqa

Automação Web utilizando Robot Framework e a lib Browser

## Estrutura de diretórios

```
web-automation-globalsqa/
 ├─ resources/
 |   ├─ actions/
 ├─ tests/
 |   └─ page/    
 └─ requirements.txt
```

- :file_folder: [resources/](resources): Dir que contém os recursos para utilizar nos testes
    - :file_folder: [actions/](actions): Dir com os arquivos que apoiam os testes
- :file_folder: [tests/](tests) : Dir que contém os testes 
    - :file_folder: [page/](page): Dir com os testes da página de Globalsqa
- :page_with_curl: [requirements.txt](requirements.txt): Arquivo com as dependencias do projeto para apoiar o CI/CD

## Branches

O projeto possui duas branches, cada uma contém:

- main: Testes utilizando cenários descritos de forma procedural 

- bdd-tests: Testes utilizando cenários descritos com o BDD

## Executando os testes

Para executar todos os testes, execute na pasta raiz:

`robot -d ./logs tests/`


